const path = require('path');

module.exports = {
	mode: 'development',
	entry: './src/index.ts',
	devServer: {
		contentBase: path.join(__dirname, 'public'),
		compress: true,
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
			{
				test: /\.css$/i,
				use: [
					{
						loader: 'style-loader',
						options: {
							injectType: 'singletonStyleTag'
						}
					},
					'css-loader'
				]
			}
		],
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js', '.d.ts'],
	},
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'public')
	}
}