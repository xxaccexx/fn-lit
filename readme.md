# FnLit

## You can do react stylish stuff with this project

But at what cost?


## Not a package, just a proof of concept.

An example App might look like this


```html
<div id="app"></div>
```

```js
import { fnLit, html, render } from '../core/fn-lit';

render(
	html`${ ExampleComponent() }`,
	document.getElementById('app')
)

const CountDisplay = fnLit(() => ({ count, setCount }) => html`
	<p>
		${count}
		<span @click="${() => setCount(count + 1)}>Increase</span>
	</p>
`);

const ExampleComponent = fnLit(({ useState, useEffect }) => 
	(props, children) => {
		const [count, setCount] = useState(0);

		return html`
			<h1>This is an example component</h1>

			${CountDisplay({ count, setCount })}

			<div>
				${children}
			</div>
		`
	}
)
```

# Pros

* Bundled code looks very similar to source.
* No magical global behind the scenes stuff, like useState, which is provided to you through closure.
* JSX is a goner, instead using html template literal strings which is more standardised in the browser.
* Async loading components is based on native es modules standard

# Cons

* Kind of verbose templates, instead of {.} -> ${.} for interperlations.
* Requires a function that takes a function that returns a function, (which also returns a function) could be bit confusing for fresh developers.
* No css-in-js, which I see as a benefit, but I understand I am not all developers.
* No XML style components... components are functions which means child content is another lit-html template.. this could cause template bloat, in medium sized usecases.

