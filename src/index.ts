// Import stylesheets
// import './style.css';
import { render } from './core/fn-lit.ts';
import { App } from './App.ts';

render(
	App,
	<HTMLElement>document.getElementById('app')
);
