import { fnLit, html, ifDefined } from '../core/fn-lit.ts';
import '../styles/button.css';

export const Button = fnLit(({ useState }) =>
	(props, children) => {
		const classes = [
			'btn',
			`btn-${ props.mode ?? 'primary' }`,
			...(props.classes ?? []),
		];

		props.size && classes.push(`btn-${ props.size }`);
		props.disabled && classes.push('disabled');
		props.outline && classes.push('btn-outline');

		const labelEl = children ?? html`
			<span>${props.label ?? 'undefined' }</span>
		`

		return html`
			<a class="${ classes.join(' ') }"
				href="${ props.href ?? '#' }"
				@click="${ ifDefined(props.onClick) }">
				${ labelEl }
			</a>
		`
	}
)