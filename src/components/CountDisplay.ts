import { fnLit, html } from '../core/fn-lit';
import { Button } from './Button';

export const CountDisplay = fnLit(() =>
	({ count, setCount }) => html`
		<p>
			Current count is at ${ count }<br>
			${ Button({ onClick: () => setCount(count + 1) }, 'Increase') }
			${ Button({ mode: 'warning', onClick: () => setCount(0) }, 'Reset') }
		</p>
	`
)