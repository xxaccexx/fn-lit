import { fnLit, html, TemplateResult } from '../core/fn-lit';
import { IStateResult } from '../core/hooks/useState';

import { CountDisplay } from './CountDisplay';

export const ExampleComponent = fnLit(({ useState }) =>
	(props: any, children: TemplateResult) => {
		const [ count, setCount ] = useState(0) as IStateResult<number>;

		return html`
		<h1>This is an example component</h1>

		${ CountDisplay({ count, setCount }) }

		<div>
			${ children }
		</div>
		`
	})
