export interface IStorageAccessor<T> {
	has: () => boolean;
	get: () => any;
	set: (v: T) => void;
}

export class Storage<T> {
	private state: Map<number, T> = new Map();
	private pointer: number = 0;

	private listeners: Set<Function> = new Set();

	getBoundAccessor(): IStorageAccessor<T> {
		const store = this.pointer++;
		return {
			has: () => this.state.has(store),
			get: () => this.state.get(store),
			set: (v: T) => {
				let o = this.state.get(store);
				this.state.set(store, v);

				if (o !== v) this.emitChange();
			},
		}
	}

	resetPointer() {
		this.pointer = 0;
	}

	onStateChange(fn: Function) {
		this.listeners.add(fn);
	}

	emitChange() {
		Promise.resolve().then(() => {
			this.pointer = 0;
			Array.from(this.listeners)
				.forEach(fn => fn.call(null));
		})
	}
}