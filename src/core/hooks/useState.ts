import { Storage } from '../storage.ts';

export type IStateResult<T> = [T, (value: T) => void];

export interface IUseState {
	(initialValue: any): IStateResult<any>
}

export type IStateClosure = (store: Storage<any>) => IUseState;

export const useState: IStateClosure = (store) =>
	<T,>(initialValue: T): IStateResult<T> => {
		const state = (<Storage<T>>store).getBoundAccessor();

		if (!state.has())
			state.set(initialValue);

		const setState = (value: T) => {
			state.set(value);
		}

		return [state.get(), setState] as IStateResult<T>;
	};