import { Storage } from '../storage.ts';

export interface IUseEffect {
	(method: () => void, once?: boolean): void;
}

export type IEffectClosure = (store: Storage<any>) => IUseEffect;

export const useEffect: IEffectClosure = (store) => (
	(method, once = false) => {
		const state = store.getBoundAccessor();

		switch (state.get()) {
			case undefined: state.set(once);
			case true: return;
		}

		setTimeout(() => method());
	}
)
