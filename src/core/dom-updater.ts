import { render as litRender } from 'lit-html';
import { ILitFunction } from './fn-lit-component.ts';

class DOMUpdater {
	private static appRoot: HTMLElement | void;
	private static appTmpl: ILitFunction | void;

	static renderEvent: string = 'update-dom';

	static updateDOM() {
		if (!DOMUpdater.appRoot || !DOMUpdater.appTmpl) return;

		Promise.resolve().then(() => {
			if (!DOMUpdater.appRoot || !DOMUpdater.appTmpl) return;
			litRender(DOMUpdater.appTmpl(), DOMUpdater.appRoot, null);
		});
	}

	static initialise(appTmpl: ILitFunction, appRoot: HTMLElement) {
		if (!DOMUpdater.appRoot && !DOMUpdater.appTmpl) {
			window.addEventListener(DOMUpdater.renderEvent, () => DOMUpdater.updateDOM());
		}

		DOMUpdater.appRoot = appRoot;
		DOMUpdater.appTmpl = appTmpl;
		DOMUpdater.updateDOM();
	}
}

export const render = DOMUpdater.initialise;