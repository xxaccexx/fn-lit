import { TemplateResult } from 'lit-html';
import { Storage } from './storage.ts';

import { useState, IUseState } from './hooks/useState.ts';
import { useEffect } from './hooks/useEffect.ts';

export interface IHooks {
	useState: IUseState;
	useEffect: any;
}

export interface ILitFunction {
	(props?: any, children?: any): TemplateResult;
}

export interface ILitComponent {
	(hooks: IHooks): ILitFunction;
}


class BaseFnLit {
	storage: Storage<any> = new Storage();

	private renderer: ILitFunction;

	constructor(component: ILitComponent) {
		this.storage.onStateChange(this.stateChanged);

		const hooks: IHooks = {
			useState: useState(this.storage),
			useEffect: useEffect(this.storage)
		}

		this.renderer = component(hooks);
	}

	get render() {
		return (...args: any[]): TemplateResult => {
			switch (args.length) {
				case 0: return this.renderer();
				case 1: return this.renderer(args[ 0 ]);
				case 2: return this.renderer(...args);

				default: {
					const props = args[ 0 ];
					const children = args[ args.length - 1 ];
					return this.renderer(props, children);
				}
			}
		}
	}

	private get stateChanged() {
		return () => {
			window.dispatchEvent(new Event('update-dom'));
		}
	}
}

export const fnLit = (component: ILitComponent): ILitFunction => {
	const instance = new BaseFnLit(component);
	return instance.render;
}