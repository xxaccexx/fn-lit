import { fnLit, html } from './core/fn-lit.ts';
import './styles/base.css';

import { ExampleComponent } from './components/ExampleComponent.ts';
import { Button } from './components/Button.ts';

const modes = [
	'primary',
	'secondary',
	'success',
	'danger',
	'warning',
	'info',
	'light',
	'dark',
	'link'
]

const sizes = [
	'sm',
	'',
	'lg',
]

const outlines = [ false, true ]

export const App = fnLit(() => () => ExampleComponent({}, html`<p>Lorem Ipsom</p>`))